package com.example.android_assignment1;

import com.google.gson.annotations.SerializedName;

public class Retrofit_Model {

    @SerializedName("_id")
    public String ID;
    @SerializedName("Resname_eng")
    public String Resname_eng;
    @SerializedName("Resname_chin")
    public String Resname_chin;
    @SerializedName("Resaddress_eng")
    public String  Resaddress_eng;
    @SerializedName("Resaddress_chin")
    public String Resaddress_chin;
    @SerializedName("Reslatitude")
    public String Reslatitude;
    @SerializedName("Reslongitude")
    public String Reslongitude;
    @SerializedName("Resphoto")
    public String Resphoto;

    public Retrofit_Model(){}
    public Retrofit_Model(String id, String engName, String chinName, String engAddress, String chinAddress,
                          String latitude, String longitude, String ResPhoto)
    {
        this.ID = id;
        this.Resname_eng = engName;
        this.Resname_chin = chinName;
        this.Resaddress_eng = engAddress;
        this.Resaddress_chin = chinAddress;
        this.Reslatitude = latitude;
        this.Reslongitude = longitude;
        this.Resphoto = ResPhoto;
    }

    public void setID(String id) {this.ID = id;}
    public void setResname_eng(String value){this.Resname_eng = value;}
    public void setResname_chin(String value){this.Resname_chin = value;}
    public void setResaddress_eng(String value){this.Resaddress_eng = value;}
    public void setResaddress_chin(String value){this.Resaddress_chin = value;}
    public void setReslatitude(String value){this.Reslatitude = value;}
    public void setReslongitude(String value){this.Reslongitude = value;}
    public void setResphoto(String value){this.Resphoto = value;}

    public String getID(){return this.ID;}
    public String getResname_eng(){return this.Resname_eng;}
    public String getResname_chin(){return this.Resname_chin;}
    public String getResaddress_eng(){return this.Resaddress_eng;}
    public String getResaddress_chin(){return this.Resaddress_chin;}
    public String getReslatitude(){return this.Reslatitude;}
    public String getReslongitude(){return this.Reslongitude;}
    public String getResphoto(){return this.Resphoto;}
}

