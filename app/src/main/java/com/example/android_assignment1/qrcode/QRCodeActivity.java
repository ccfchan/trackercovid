package com.example.android_assignment1.qrcode;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.android_assignment1.MainActivity;
import com.example.android_assignment1.R;
import com.example.android_assignment1.map.MapsActivity;
import com.example.android_assignment1.restaurant.RestaurantActivity;
import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QRCodeActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    ZXingScannerView zXingScannerView;
    //Nav bar
    public Button btnHone, btnShop, btnMap, btnMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        zXingScannerView = findViewById(R.id.ZXingScannerView_QRCode);
        //Get permission of the camera
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                ActivityCompat.checkSelfPermission(this
                        , Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
                    100);
        } else {
            //If permission already given, directly open the camera
            openQRCamera();
        }

        //Nav bar
        btnHone = findViewById(R.id.btnHome);
        btnShop = findViewById(R.id.btnShop);
        btnMap = findViewById(R.id.btnMap);
        btnMenu = findViewById(R.id.btnMenu);
    }

    //Open the camera
    private void openQRCamera() {
        zXingScannerView.setResultHandler(this);
        zXingScannerView.startCamera();
    }

    //Get the request permission result
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100 && grantResults[0] == 0) {
            openQRCamera();
        } else {
            Toast.makeText(this, "No Permission", Toast.LENGTH_SHORT).show();
        }
    }

    //Stop camera
    @Override
    protected void onStop() {
        zXingScannerView.stopCamera();
        super.onStop();
    }

    //Get the object from the QR code
    @Override
    public void handleResult(Result rawResult) {
        //Check the QR code saved URL (which will direct to photo saved)
        if (Patterns.WEB_URL.matcher(rawResult.getText()).matches()) {
            //Intent to broswer
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(rawResult.getText()));
            startActivity(browserIntent);
        }
        //If no designed qr code scanned, continue scanning
        openQRCamera();
    }

    //Nav bar
    public void ToHome(View view) {
        Intent i = new Intent(QRCodeActivity.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    public void ToShop(View view) {
        Intent i = new Intent(QRCodeActivity.this, RestaurantActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    public void ToMap(View view) {
        Intent i = new Intent(QRCodeActivity.this, MapsActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    public void ToNutLab(View view) {
        finish();
        startActivity(getIntent());
    }
}
