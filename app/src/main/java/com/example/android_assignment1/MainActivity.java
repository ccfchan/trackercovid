package com.example.android_assignment1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.android_assignment1.map.MapsActivity;
import com.example.android_assignment1.qrcode.QRCodeActivity;
import com.example.android_assignment1.restaurant.RestaurantActivity;

public class MainActivity extends AppCompatActivity{
    public Button btnHone, btnShop, btnMap, btnMenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Nav bar
        btnHone = findViewById(R.id.btnHome);
        btnShop = findViewById(R.id.btnShop);
        btnMap = findViewById(R.id.btnMap);
        btnMenu = findViewById(R.id.btnMenu);
    }

    //Nav bar
    public void ToHome(View view) {
        finish();
        startActivity(getIntent());
    }

    public void ToShop(View view) {
            Intent i = new Intent(MainActivity.this, RestaurantActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
    }

    public void ToMap(View view) {
        Intent i = new Intent(MainActivity.this, MapsActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    public void ToNutLab(View view) {
        Intent i = new Intent(MainActivity.this, QRCodeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }
}