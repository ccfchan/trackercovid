package com.example.android_assignment1.map;

import android.util.Log;

import com.example.android_assignment1.Get_Request_Interface;
import com.example.android_assignment1.Get_Request_Path_Interface;
import com.example.android_assignment1.Retrofit_Model;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MapRepository implements MapContact.Model {

    @Override
    public void loadLocation(MapsPresenter mapsPresenter) {
        //Call data from Retrofit Model
        Get_Request_Interface request = Get_Request_Path_Interface.retrofit.create(Get_Request_Interface.class);
        //getAllStore
        Call<List<Retrofit_Model>> call = request.getAllStore();

        call.enqueue(new Callback<List<Retrofit_Model>>() {
            @Override
            public void onResponse(Call<List<Retrofit_Model>> call, Response<List<Retrofit_Model>> response) {
//                Log.d("response " + response.body());
//                Log.d("locale", Locale.getDefault().getDisplayLanguage());
                if (response.code() == 200) {
                    //Check the language of the device
                    if (Locale.getDefault().getDisplayLanguage().equals("中文")) {
                        //Chinese
                        for (int i = 0; i < response.body().size(); i++) {
                            Location location = new Location(Double.parseDouble(response.body().get(i).Reslatitude), Double.parseDouble((response.body()).get(i).Reslongitude));
                            mapsPresenter.getLocation((response.body()).get(i).Resaddress_chin, location);
                        }
                    } else {
                        //English
                        for (int i = 0; i < response.body().size(); i++) {
                            Location location = new Location(Double.parseDouble(response.body().get(i).Reslatitude), Double.parseDouble((response.body()).get(i).Reslongitude));
                            mapsPresenter.getLocation((response.body()).get(i).Resaddress_eng, location);
                        }
                    }
                } else {
                    Log.i("","error message");
                    //Testing
                    mapsPresenter.displayToastMessageOnView("Connect database error");
                }
            }

            @Override
            public void onFailure(Call<List<Retrofit_Model>> call, Throwable t) {
//                System.out.println("response error " + t);
                //Testing
                Log.i("","error message");
                mapsPresenter.displayToastMessageOnView("Fail to connect to database");
            }
        });
    }

}
