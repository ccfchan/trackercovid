package com.example.android_assignment1.map;


import com.example.android_assignment1.Retrofit_Model;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;


public class MapContact {

    //Presenter
    interface Presenter{
        public void onMapReady();
        public void getLocation(String licensePlate, Location location);
        //Testing
        public void displayToastMessageOnView(String msg);
    }

    //Model
    interface Model {
        public void loadLocation(MapsPresenter presenter);
    }

    //View
    public interface mapView {
        public void addScooter(String licensePlate, LatLng location);
        public void scrollMapTo(LatLng location);
        //Testing
        public void displayToastMessage(String msg);
    }
}
