package com.example.android_assignment1.map;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.android_assignment1.MainActivity;
import com.example.android_assignment1.R;
import com.example.android_assignment1.qrcode.QRCodeActivity;
import com.example.android_assignment1.restaurant.RestaurantActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, MapContact.mapView{

    private GoogleMap mMap;

    private MapsPresenter mapsPresenter;
    private MapRepository mapRepository;

    //Nav bar
    public Button btnHone, btnShop, btnMap, btnMenu;

    //Camera center
    LatLng hk = new LatLng(22.364781, 114.083440);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mapRepository = new MapRepository();
        mapsPresenter = new MapsPresenter(this, mapRepository);

        //Nav bar
        btnHone = findViewById(R.id.btnHome);
        btnShop = findViewById(R.id.btnShop);
        btnMap = findViewById(R.id.btnMap);
        btnMenu = findViewById(R.id.btnMenu);

        //Request permission for map
        //Testing
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mapsPresenter.onMapReady();
        scrollMapTo(hk);
    }


    @Override
    public void addScooter(String licensePlate, LatLng location) {
        //Add all stores' marker
        mMap.addMarker(new MarkerOptions().position(location).title(licensePlate));
    }

    @Override
    public void scrollMapTo(LatLng location) {
        //Move camera and zoom in
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hk, 11));
    }

    //Testing
    @Override
    public void displayToastMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    //Nav bar
    public void ToHome(View view) {
        Intent i = new Intent(MapsActivity.this, MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    public void ToShop(View view) {
        Intent i = new Intent(MapsActivity.this, RestaurantActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    public void ToMap(View view) {
        finish();
        startActivity(getIntent());
    }

    public void ToNutLab(View view) {
        Intent i = new Intent(MapsActivity.this, QRCodeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }
}