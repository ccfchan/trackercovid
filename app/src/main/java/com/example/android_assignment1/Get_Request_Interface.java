package com.example.android_assignment1;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Get_Request_Interface {
    //For get all store info, route
    @GET("/location")
    Call<List<Retrofit_Model>> getAllStore();
}
