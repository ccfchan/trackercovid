package com.example.android_assignment1.restaurant;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.android_assignment1.R;
import com.example.android_assignment1.Retrofit_Model;

import java.util.List;
import java.util.Locale;

public class RestaurantRecyclerViewAdapter extends RecyclerView.Adapter<RestaurantRecyclerViewAdapter.ViewHolder> {

    private List<Retrofit_Model> mData;
    RestaurantPresenter restaurantPresenter;
    Context mContext;

    RestaurantRecyclerViewAdapter(List<Retrofit_Model> data, RestaurantPresenter infoPresenter, Context context) {
        mData = data;
        restaurantPresenter = infoPresenter;
        mContext = context;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView store_name, store_address;

        ViewHolder(View itemView) {
            super(itemView);
            store_name = (TextView) itemView.findViewById(R.id.store_name);
            store_address = (TextView) itemView.findViewById(R.id.store_address);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Link the shop_list
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.restaurant_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Retrofit_Model obj = mData.get(position);
//        System.out.println("mData: " + mData);
//        mStoreInfo.add(obj.ID);

//        System.out.println(Locale.getDefault().getDisplayLanguage());
        //Use glide for URL to photo
        //Check the language of the device
        if (Locale.getDefault().getDisplayLanguage().equals("中文")) {
            //Chinese
            holder.store_name.setText(obj.Resname_chin);
            holder.store_address.setText(obj.Resaddress_chin);
        } else {
            //English
            holder.store_name.setText(obj.Resname_eng);
            holder.store_address.setText(obj.Resaddress_eng);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
