package com.example.android_assignment1.restaurant;
import com.example.android_assignment1.Retrofit_Model;

import java.util.List;

public class RestaurantPresenter {
    private RestaurantRespository restaurantRespository;
    private View view;

    public RestaurantPresenter(View view) {
        this.view = view;
        this.restaurantRespository = new RestaurantRespository();
    }


    public void callAPI() {
        //Show all record
        //Call
        restaurantRespository.API(this);
        System.out.println("Call API");
    }

    public void getResponseSuccess(List<Retrofit_Model> body){
        view.addToView(body);
    }

    public interface View {
        void addToView(List<Retrofit_Model> list);
        void displayToastMessage(String msg);
    }

    public void displayToastMessageOnView(String msg) {view.displayToastMessage(msg); }










}
