package com.example.android_assignment1.restaurant;

import android.util.Log;

import com.example.android_assignment1.Get_Request_Interface;
import com.example.android_assignment1.Get_Request_Path_Interface;
import com.example.android_assignment1.Retrofit_Model;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RestaurantRespository {
    public void API(RestaurantPresenter restaurantPresenter) {
        //Call data from Retrofit Model
        Get_Request_Interface request = Get_Request_Path_Interface.retrofit.create(Get_Request_Interface.class);
        //getAllStore
        Call<List<Retrofit_Model>> call = request.getAllStore();

        call.enqueue(new Callback<List<Retrofit_Model>>() {
            @Override
            public void onResponse(Call<List<Retrofit_Model>> call, Response<List<Retrofit_Model>> response) {
//                System.out.println("response " + response.body());
                if (response.code() == 200) {
                    restaurantPresenter.getResponseSuccess(response.body());
//                    System.out.println("response " + response.body());
                } else {
                    //Testing
                    Log.d("","error msg1");
//                    restaurantPresenter.displayToastMessageOnView("Connect database error");
                }
            }

            @Override
            public void onFailure(Call<List<Retrofit_Model>> call, Throwable t) {
//                System.out.println("response error " + t);
                //Testing
                Log.d("","error msg2");
//                restaurantPresenter.displayToastMessageOnView("Fail to connect to database");
            }
        });
    }
}
