package com.example.android_assignment1.restaurant;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android_assignment1.R;
import com.example.android_assignment1.Retrofit_Model;

import java.util.List;

public class RestaurantActivity extends AppCompatActivity implements RestaurantPresenter.View {
    private RecyclerView recycler_view;
    private RestaurantRecyclerViewAdapter adapter;
    private RestaurantPresenter restaurantPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        restaurantPresenter = new RestaurantPresenter(this);
        restaurantPresenter.callAPI();



        //RecyclerView
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view_shopInfo);
    }

    @Override
    public void addToView(List<Retrofit_Model> list) {
        //Set as list
        recycler_view.setLayoutManager(new LinearLayoutManager(this));
        //Set grid
        recycler_view.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        //To adapter
        adapter = new RestaurantRecyclerViewAdapter(list, restaurantPresenter, this);
        //Adapter to RecyclerView
        recycler_view.setAdapter(adapter);

    }

    @Override
    public void displayToastMessage(String msg) {

    }
}