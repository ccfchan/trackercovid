package com.example.android_assignment1;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public interface Get_Request_Path_Interface {

    //for Physical Device + Virtual Device (using WIFI)
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://192.168.2.147:3000")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
